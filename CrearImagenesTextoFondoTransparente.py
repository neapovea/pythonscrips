# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import os.path
import sys

import csv
from PIL import Image, ImageDraw, ImageFont



def create_image(size, bgColor, message, font, fontColor, message2, font2, fontColor2):
    W, H = size
    image = Image.new('RGB', size, color=bgColor)
    draw = ImageDraw.Draw(image)
    image.putalpha(0)

    _, _, w, h = draw.textbbox((0, 0), message, font=font)
    draw.text(((W-w)/2, (H-h)/2 -45), message, font=font, fill=fontColor)

    _, _, w, h = draw.textbbox((0, 0), message2, font=font2)
    draw.text(((W - w) / 2, (H - h) / 2 +45), message2, font=font2, fill=fontColor2)


    return image


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

    ##https://pillow.readthedocs.io/en/stable/handbook/concepts.html#transparency

    ## 7740 C  #368f3f  (54, 143, 63)
    ## 356 C   #007932  (0, 121, 50)
    ## Black C #2e2925  (46, 41, 37)

    #NotoSansMono-Bold.ttf
    #NotoSansMono-Regular.ttf

    destinationPath="/home/alex/Descargas/tmpVideos/"
    fontName="NotoSansMono-Bold.ttf"



    imageBGColor=(255, 255, 255, 127)

    text1Color = (46, 41, 37)
    text1Font = ImageFont.truetype(fontName, 55)
    text2Color = (54, 143, 63)
    text2Font = ImageFont.truetype(fontName, 55)



    ##ImageDraw.text(xy, text, fill=None, font=None, anchor=None, spacing=4, align='left', direction=None, features=None, language=None, stroke_width=0, stroke_fill=None, embedded_color=False)

    with open('lista.csv', newline='') as File:
        reader = csv.reader(File, delimiter=';')
        for row in reader:
            print(row)



            xSize = 1920
            ySize = 1080
            xPosition = 0
            yPosition = 0

            fileNane = destinationPath+row[1]+"_"+row[2]+".png"

            text1 = row[0]
            text2 = row[2]
            #text3 = row[3]

            myImage = create_image((xSize, ySize), imageBGColor, text1, text1Font, text1Color  , text2, text2Font, text2Color     )

            myImage.save(fileNane, "PNG")

            # img = Image.new(mode="RGBA", size=(1920, 1080), color=(255, 255, 255, 127))
            # draw = ImageDraw.Draw(img)

            # text1 = row[0]
            # txtSize1 = len(text1) * 25
            # text2 = row[2]
            # txtSize2 = len(text2) * 25
            # xPosition = (xSize / 2) - (txtSize1 / 2)
            # print(xPosition)
            # print(txtSize1)
            # yPosition = (ySize - 75) / 2
            # draw.text((xPosition, yPosition), text1, font=fnt, fill=(46, 41, 37))
            #
            # xPosition = (xSize / 2) - (txtSize2 / 2)
            # print(xPosition)
            # print(txtSize2)
            # yPosition = (ySize + 75) / 2
            # draw.text((xPosition, yPosition), text2, font=fnt2, fill=(54, 143, 63))
            #
            # img.save(fileNane, "PNG")



def leerCsv():
    with open('lista.csv', newline='') as File:
        reader = csv.reader(File, delimiter=';')
        for row in reader:
            print(row)
        return reader


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    #leerCsv()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
